import { View, Text, Modal, TouchableWithoutFeedback, Animated, TouchableOpacity, } from 'react-native'
import React from 'react'
import { SimpleLineIcons } from '@expo/vector-icons'; 
import { Colors } from 'react-native/Libraries/NewAppScreen'

const FilterModal = ({isVisible, onClose}) => {

    const [showFilterModal, setShowFilterModal] = React.useState(isVisible)
    const modalAnimatedValue = React.useRef(new Animated.Value(0)).current

    React.useEffect(() => {
        if(showFilterModal){
            Animated.timing(modalAnimatedValue, {
                toValue: 1,
                duration: 500, 
                useNativeDriver: false
             }).start();
        } else{
            Animated.timing(modalAnimatedValue, {
                toValue: 0,
                duration: 500, 
                useNativeDriver: false
        }).start(() => onClose());
        }
    }, [showFilterModal])

    const modalY= modalAnimatedValue.interpolate({
        inputRange:[0,1],
        outputRange: [1000, 90]
    })

    return (
    <Modal
        animationType="fade"
        transparent = {true}
        visible = {isVisible}
    >
        <View style = {{flex:1, backgroundColor:'rgba(52, 52, 52, 0.7)'}}>
        {/*Transparent background*/}
        <TouchableWithoutFeedback onPress={() => setShowFilterModal(false)}>
            <View style={{position:'absolute', top:0, right:0,left:0, bottom:0}}></View>
        </TouchableWithoutFeedback>

        <Animated.View style={{position:'absolute', left:0,top: modalY, width:"100%",height:"100%",borderTopRightRadius:30,borderTopLeftRadius:30, backgroundColor:"#FDFDFD"}}>
            {/*Header*/}
            <View style={{flexDirection:'row', alignItems:'center', marginHorizontal:"10%", height:40}}>
                <Text style={{flex:1, fontSize:18, fontWeight:'bold',bottom:0}}> Filtreraj iskanje</Text>
                <TouchableOpacity onPress={() =>setShowFilterModal(false)}>
                    <SimpleLineIcons name="arrow-down" size={20} color="black" />
                </TouchableOpacity>
            </View>
        </Animated.View>
        </View>
    </Modal>
  )
}

export default FilterModal