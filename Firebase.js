// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getAnalytics } from "firebase/analytics";
import { getFirestore } from "firebase/firestore";
import { getAuth } from "firebase/auth"
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyC1lgAxFB_8bAGp3Z2a77eFpD-FhuMoUVI",
  authDomain: "ulajf-31d15.firebaseapp.com",
  projectId: "ulajf-31d15",
  storageBucket: "ulajf-31d15.appspot.com",
  messagingSenderId: "397938701989",
  appId: "1:397938701989:web:07820359c1ce8094723f4c",
  measurementId: "G-WV4QECPMRQ"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const analytics = getAnalytics(app);
const auth = getAuth();
const db = getFirestore();

export { auth, db }