import { View, Text } from 'react-native'
import React from 'react'
import { createNativeStackNavigator } from '@react-navigation/native-stack'
import HomeScreen from './Screens/HomeScreen';
import LoginScreen from './Screens/LoginScreen';
import ChatScreen from './Screens/ChatScreen';
import UseAuth from './Hooks/UseAuth';
import ModalScreen from './Screens/ModalScreen';
import FilterModal from './Components/FilterModal';

const Stack= createNativeStackNavigator();


const StackNavigator = ()  => {
    const {user} = UseAuth();
  return (
    <Stack.Navigator
    screenOptions={{
      headerShown:false,
    }}
    >
    {user ?(
        <>
            <Stack.Group>
            <Stack.Screen name="Home" component={HomeScreen} />
            </Stack.Group>

            <Stack.Group screenOptions={{ presentation: 'modal' }}>
              <Stack.Screen name="Modal" component={ModalScreen} />
            </Stack.Group>
        </>
    ) : (
        <Stack.Screen name="Login" component={LoginScreen} />
    )}
    </Stack.Navigator>
  )
}

export default StackNavigator