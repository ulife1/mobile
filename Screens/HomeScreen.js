import { View, Text, Button, TouchableOpacity, Image, StyleSheet} from 'react-native';
import React, { useLayoutEffect } from 'react'
import { useNavigation } from '@react-navigation/native'
import UseAuth from '../Hooks/UseAuth';
import { SafeAreaView } from 'react-native-safe-area-context';
import {useTailwind} from 'tailwind-rn';
import { AntDesign, Octicons ,Ionicons, FontAwesome, Feather, MaterialIcons } from '@expo/vector-icons'; 
import { Entypo } from '@expo/vector-icons'; 
import Swiper from 'react-native-deck-swiper';
import { Divider } from 'react-native-elements';
import FilterModal from '../Components/FilterModal';

const DUMMY_DATA =[
  {
    eventname: "LUKA BASI, POLKAHOLIKI, TIL ČEH",
    date: "SOBOTA, 18.06.2022",
    timestart: "20:00",
    timeend: "05:00",
    location: "DUPLEK, DUPLEK",
    imageURL: "https://cmc.com.hr/wp-content/uploads/2021/12/luka-basi-to-je-to.jpg",
    id:12,
  },
  {
    eventname: "MAGNIFICO JE BEST",
    date: "PONEDELJEK, 01.07.2022",
    timestart: "18:00",
    timeend: "02:00",
    location: "ŽUPNA, GROSUPLJE",
    imageURL: "https://images.squarespace-cdn.com/content/v1/6088c14a5641d425b6e5e013/1648025998678-V8ECLSWMRNXKYKKR1K4C/image-asset.jpeg",
    id:23,
  },
  {
    eventname: "SIDDHARTA",
    date: "PETEK, 23.11.2022",
    timestart: "21:00",
    timeend: "02:00",
    location: "KOPER, KOPER",
    imageURL: "https://lokalec.si/wp-content/uploads/2013/08/Zajeta-slika.png",
    id:4,
  },
  {
    eventname: "JOKER OUT ALL NIGHT",
    date: "SOBOTA, 18.06.2022",
    timestart: "00:00",
    timeend: "07:00",
    location: "PTUJ, PTUJ",
    imageURL: "https://www.pomurec.com/data/galerija/21/06/da39a3ee5e6b4b0d3255bfef95601890afd80709/31625059576Joker_out-Matic_Kremzar-1%20(2).jpg",
    id:16,
  }
]

const HomeScreen = () => {
    const tw = useTailwind();
    const navigation = useNavigation();
    const {user, logout} = UseAuth();

    const [showFilterModal, setShowFilterModal] = React.useState(false);

    useLayoutEffect(() => {
      navigation.setOptions({
        headerShown: false,
      })
    }, [])

  return (
    <SafeAreaView style={{flex:1, backgroundColor:"#FDFDFD"}}>

      {/* Header */}
      <View style={{ alignItems:'center'}}>

        <TouchableOpacity style={[, {position: 'absolute', Top:25, left:5, padding:17,}]}
        onPress={logout}
        >
          <Image style={{borderRadius:30, height:45, width:45}} source={ {uri: user.photoURL}} />
        </TouchableOpacity>
        
        <TouchableOpacity style={{ Top:10,padding:4}}>
          <Entypo name="bowl" size={34} color="black" />
        </TouchableOpacity>
        <TouchableOpacity style={[{justifyContent:'center',alignItems:'center',width:45,height:45,borderRadius:30,position:'absolute', top:15, right:25,padding:0, backgroundColor:"white"},styles.filtershadow]}
        onPress={() => setShowFilterModal(true)}
        >
          <Feather name="filter" size={24} color="#818181" />
        </TouchableOpacity>
        {showFilterModal &&
          <FilterModal 
            isVisible = {showFilterModal}
            onClose = {() => setShowFilterModal(false)}
          />
        }
      </View>
      {/* End of Header */}

      {/* Background no more events */}

      {/* End of Background no more events */}
      {/* Cards */}
      <View style={{flex:1,marginTop:2}}>
        <Swiper 
            containerStyle={{ backgroundColor:"transparent" }}
            cards={DUMMY_DATA}
            stackSize={5}
            cardIndex={0}
            animateCardOpacity
            verticalSwipe={false}
            onSwipedLeft = {() => {
                console.log('Swipe going')
            }}
            onSwipedRight = {() => {
              console.log('Swipe pass')
          }}
            overlayLabels = {{
              left: {
                title: "GOING",
                style: {
                  label: {
                    textAlign: "right",
                    color: "green",
                  },
                },
              },
              right: {
                title: "NOPE",
                style: {
                  label: {
                    textAlign: "left",
                    color: "red",
                  }
                }
              }
            }}
            renderCard={(card) => (
              <View key={card.id} style={{alignSelf:"center", backgroundColor:"white", height:"60%",width:"90%",borderRadius:5}}>

                <Image style={[tw("h-full"),{top:0,borderTopLeftRadius:6,borderTopRightRadius:6}]} source={{uri: card.imageURL}} />

                <View style={[styles.cardshadow, {backgroundColor:"white", width:"100%", borderBottomRightRadius: 6,borderBottomLeftRadius:6}]}>
                  <View>
                    <Text style={{paddingTop:4,paddingLeft:10,fontWeight:"bold"}}>
                      {card.eventname}
                    </Text>
                    <Text style={{fontSize:10,paddingTop:0,paddingLeft:10}}>
                      {card.date}, {card.timestart} - {card.timeend}
                    </Text>
                    <Text style={{fontSize:12,paddingTop:12,paddingBottom:5,paddingLeft:10}}>
                      {card.location}
                    </Text>
                  </View>                 
                </View>
              </View>

            )}
          />
      </View>
      {/* End of Cards */}
      {/*Footer*/}
      <View style={{height:63, backgroundColor:"white", borderTopWidth:1, borderTopColor:"#F0F2F1"}}>
        <View style={{justifyContent:'space-between',flexDirection:'row',marginHorizontal:"7%",bottom:3,top:2}}>

          <TouchableOpacity style={{width:60,alignItems:"center", justifyContent:'center',}}
          onPress={() => navigation.navigate("Home")}
          >
            <Feather name="home" size={25} color="#818181" />
          </TouchableOpacity>

          <TouchableOpacity style={{width:60,alignItems:"center", justifyContent:'center',}}
          onPress={() => navigation.navigate("Home")}
          >
            <Ionicons name="search" size={25} color="#818181" />
          </TouchableOpacity>
            
          <TouchableOpacity style={{height:57,width:57, backgroundColor:"#B0D5FF",alignItems:"center", justifyContent:'center', borderRadius:30}}
          onPress={() => navigation.navigate("Modal")}
          >
            <AntDesign name="plus" size={30} color="white" />
          </TouchableOpacity>

          <TouchableOpacity style={{width:60,alignItems:"center", justifyContent:'center',}}
          onPress={() => navigation.navigate("Home")}
          >
            <AntDesign name="message1" size={25} color="#818181" />
          </TouchableOpacity>

          <TouchableOpacity style={{width:60,alignItems:"center", justifyContent:'center',}}
          onPress={() => navigation.navigate("Home")}
          >
            <MaterialIcons name="event-note" size={25} color="#818181" />
          </TouchableOpacity>

        </View>
      </View>
      {/*End of Footer*/}
      <Divider width={1} />
    </SafeAreaView>

    
  );
};

export default HomeScreen

const styles = StyleSheet.create({
  cardshadow: {
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.2,
    shadowRadius: 1.41,

    elevation: 2,
  },
  filtershadow: {  
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.2,
    shadowRadius: 1.41,

    elevation: 5,
  }
  
})