import { View, Text, Button, ImageBackgrounds, TouchableOpacity, Image, ImageBackground } from 'react-native'
import React, { useLayoutEffect } from 'react'
import UseAuth from '../Hooks/UseAuth'
import { useNavigation } from '@react-navigation/native';
import {TailwindProvider, useTailwind} from 'tailwind-rn';
import { SafeAreaView } from 'react-native-safe-area-context';
import { TwitterAuthProvider } from 'firebase/auth';

const LoginScreen = () => {
    const tw = useTailwind();
    const { signInWithGoogle, loading } = UseAuth();
    const navigation = useNavigation();

    useLayoutEffect(() => {
      navigation.setOptions({
        headerShown: false,
      })
    }, [])
  return (
    <View style={tw("flex-1")}>

      <ImageBackground
      resizeMode="cover"
      style={tw("flex-1")}
      source={require('../Images/LoginBackground.jpg')}
      >
        <Text style={[ {marginHorizontal: "25%", marginTop:"50%",fontWeight:'bold', alignSelf:"center", fontSize:80, fontFamily:'Roboto'}]}>uLajf</Text>
        <TouchableOpacity
          style={[tw("absolute bottom-40 w-52 p-4 rounded-2xl"), {marginHorizontal: "25%", backgroundColor:"#B0D5FF"}]}
        >
          <Text style={tw("text-center")}>
            Sign in
          </Text>
        </TouchableOpacity>        
        <TouchableOpacity style={[tw("rounded-2xl absolute bottom-20"),{height: 40, width:40, alignSelf:'center'}]}
        onPress= {signInWithGoogle}
        >
        <Image style={[{height: 40, width:40, }]} 
          source={require('../Images/google.png')}/>
        </TouchableOpacity>
      </ImageBackground>
    </View>
  );
};

export default LoginScreen 